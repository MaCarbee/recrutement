import { createRouter, createWebHistory } from 'vue-router'
import { authGuard } from '@/services/auth-guard'
import LoginPage from '@/views/LoginPage'
import Signup_Page from '@/views/Signup_Page'
import Home_Page from '@/views/Home_Page'
import Signup_Page_Recruiteur from '@/views/Signup_Page_Recruiteur'
import LoginPage_recruteur from '@/views/LoginPage_recruteur'
import ForgotPassword from '@/views/auth/ForgotPassword.vue'
import NewPassword from '@/views/auth/NewPassword.vue'
import choice_role from '@/views/choice_role'
import ProfilRecruteur from '@/views/recruteur/ProfilRecruteur.vue'
import ProfilCandidat from '@/views/candidat/ProfilCandidat'
import DashboardRecruteur from '@/views/recruteur/DashboardView.vue'
import DashboardCandidat from '@/views/candidat/DashboardView.vue'
import ExperiencesView from '@/views/candidat/ExperiencesView.vue'
import AddExperienceView from '@/views/candidat/AddExperienceView.vue'
import EditExperienceView from '@/views/candidat/EditExperienceView.vue'
import StatistiqueView from '@/views/recruteur/StatistiqueView.vue'
import CreateOffreView from '@/views/recruteur/offre_emploi/CreateOffreView.vue'
import OffreDisponible from '@/views/recruteur/offre_emploi/OffreDisponible.vue'
import DetailOffre from '@/views/recruteur/offre_emploi/DetailOffre.vue'

import EditOffreView from '@/views/recruteur/offre_emploi/EditOffreView.vue'
const routes = [
    {
        path: '/',
        name: 'Home_Page',
        component: Home_Page
    },
    {
        path: '/candidat/register',
        name: 'Signup_Page',
        component: Signup_Page
    },
    {
        path: '/candidat/login',
        name: 'LoginPage',
        component: LoginPage
    },
    {
        path: '/Recruteur/register',
        name: 'register_recruiteur',
        component: Signup_Page_Recruiteur
    },
    {
        path: '/Recruteur/login',
        name: 'login_recruiteur',
        component: LoginPage_recruteur
    },
    {
        path: '/utilisateur',
        name: 'role',
        component: choice_role
    },
    {
        path: '/dashboard-recruteur',
        name: 'dashboard_recruteur',
        component: StatistiqueView,
        meta:{requiresAuth:true, type:'recruteur'}
    },
    {
        path: '/dashboard-recruteur/profil',
        name: 'profil_recruteur',
        component: ProfilRecruteur,
        meta:{requiresAuth:true,type:'recruteur'}
    },
    {
        path: '/dashboard-recruteur/add_offre',
        name: 'add_offre',
        component: CreateOffreView,
        meta:{requiresAuth:true, type:'recruteur'}
    },
    {
        path:'/dashboard-recruteur/edit_offre/:id',
        name:'edit_offre',
        component:EditOffreView,
        props:true,
        meta:{requiresAuth:true,type:'recruteur'}
    },

    {
        path: '/dashboard-candidat/profil',
        name: 'profil_candidat',
        component: ProfilCandidat,
        meta:{requiresAuth:true,type:'candidat'}
    },
    {
        path: '/dashboard-candidat/experiences',
        name: 'experiences',
        component: ExperiencesView,
        meta:{requiresAuth:true,type:'candidat'}
    },
    {
        path: '/dashboard-candidat/add_experience',
        name: 'add_experience',
        component: AddExperienceView,
        meta:{requiresAuth:true,type:'candidat'}

    },
    {
        path: '/dashboard-candidat/edit_experience/:exp_id',
        name: 'edit_experience',
        component: EditExperienceView,
        props: true,
        meta:{requiresAuth:true,type:'candidat'}
    },
    {
        path: '/forgot_password',
        name: 'forgot_password',
        component: ForgotPassword
    },
    {
        path:'/recruteur/DetailOffre',
        name:'DetailOffre',
        component:DetailOffre,
    },
    {
        path:'/recruteur/offre_disponible',
        name:'OffreDisponible',
        component:OffreDisponible,
    },

    {
        path: '/password-reset-confirm/:user_id/:token',
        name: 'reset_password',
        component: NewPassword,
        props: true
    },
    {
        path: '/:pathMatch(.*)*', redirect: '/'
    }
]
const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

// router.beforeEach((to,from,next)=>{
//         if(to.meta.requiresAuth && to.meta.type == 'candidat'){
//             authGuard('candidat')
//         }else if(to.meta.requiresAuth && to.meta.type == 'recruteur'){
//             authGuard('recruteur')
//         }
//         next()
// })

export default router