import axios from 'axios'
import store from '@/store'
import { accountService } from './account.service'
const Axios = axios.create({
    baseURL:'http://localhost:8000'
})

Axios.interceptors.request.use(request=>{
   const token = accountService.TOKEN
    if(token){
        request.headers.Authorization= 'Bearer '+token
    }
    return request

})
export default Axios
