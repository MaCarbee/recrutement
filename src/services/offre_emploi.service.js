import Axios from "./caller.service";

let create_offre=(data)=>{
    return Axios.post('api/offre_emploi/',data,{headers:{'Content-Type':'multipart/form-data'}})
}

let update_offre=(data,offre_id)=>{
    return Axios.put(`api/offre_emploi/${offre_id}/`,data,{headers:{'Content-Type':'multipart/form-data'}})
}

let get_offre = (offre_id)=>{
    return Axios.get(`api/offre_emploi/${offre_id}/`)
}

let delete_offre = (offre_id)=>{
    return Axios.delete(`api/offre_emploi/${offre_id}/`)
}

let get_recruteur_offres=(recruteur_id)=>{
    return Axios.get(`api/offre_emploi/${recruteur_id}/user_recrutements/`)
}


export const OffreEmploi ={create_offre,update_offre,get_offre,get_recruteur_offres,delete_offre}