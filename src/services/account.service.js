import Axios from './caller.service'
import store from '@/store/index'

const TOKEN = store.getters.getToken
const USER_ROLE= store.getters.getRole
const USER= store.getters.getUser
let register = (credentials)=>{
    return Axios.post('api/register/',{
        email:credentials.email,
        password:credentials.password,
        role:credentials.role
    
    })
}


let login =(credentials)=>{
    return Axios.post('/api/token/', {
        email: credentials.email,
        password: credentials.password,
        role:credentials.role
    })
}

let logout = ()=>{
    store.commit('SET_TOKEN',null)
    store.commit('SET_USER',null)
}



let saveUser= (user)=>{
    store.commit('SET_USER',user)
}

let saveToken = (token)=>{
    store.commit('SET_TOKEN',token)
}

let sendForgotPasswordMail = (credentials)=>{
    return Axios.post('api/password-reset/',{email:credentials.email})
}

let reset_password =(payload)=>{
    const url = `api/password-reset-confirm/${payload.user_id}/${payload.token}/`
    return Axios.post(url,{password:payload.password})
}

let update_profil_recruteur = (data)=>{
    return Axios.put('api/recruteur/update/',data,{headers:{'Content-Type':'multipart/form-data'}})
}
let update_profil_candidat = (data)=>{
    return Axios.put('api/candidat/update/',data,{headers:{'Content-Type':'multipart/form-data'}})
}

let get_profil_recruteur =()=>{
    return Axios.get('api/recruteur/update/')
}
let get_profil_candidat =()=>{
    return Axios.get('api/candidat/update/')
}
export const accountService = {
    login,logout,saveUser,saveToken,register, TOKEN,USER_ROLE,USER,
    sendForgotPasswordMail,reset_password,update_profil_recruteur,
    get_profil_recruteur,get_profil_candidat,update_profil_candidat,
}

