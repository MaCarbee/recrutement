import { accountService } from './account.service'
import router from '@/router'


export function authGuard(role){
    const token  = accountService.TOKEN
    const user_role = accountService.USER_ROLE
    if(token){
        if(user_role == role){
            console.log('cool');
            return true
        }else{
            // router.push({name:`dashboard_${user_role}`})
            router.push({name:'role'})
        }
    }else{
        router.push({name:'Home_Page'})
    }
    
}